# libli-app

A libli-app renders the app's layout and router, used on libli.org, it
is the main entry point to using and customizing libli on a webpage.

It gets its configurations from its HTML attributes.

# Attributes

The example site uses the code in the `index.html` file as entry
point. It fetches from the static `.env.json` public file (see
`/assets/.env*.json` for how this data is stored for modularity of
environments).

The URL related attributes should be similar in values as the keys
with the same names to `window.location` of the page Libli is served
from (our homepage with libli on it).

## origin

The base URL, homepage, `origin` of our site, such as
`https://libli.org` for the example instance.

> In the case libli is server from a sub-folder, such as
> `https://example.org/libli/` use `https://example.org` as the
> origin, as the rest will go into the `hostname` attribute.

## pathname

The base "path" of the subfolder from which libli is served in the
HTML page.

In the example instance, since `libli-app` is server from
`https://libli.org/index.html`, the `pathname` value is `/` (to
represent the "root folder" of our server).

In the case of libli served from the `https://example.org/libli/`
subfolder, we would use `/libli/` as value (with the trailing slash,
wich could otherwise result in bugs when links are clicked or URL
resolved by libli's router).

## homeserver-authorized

By default, libli is not authorizing API calls made to any matrix
homeserver.

The value of `homeserver-authorized` is a JSON Array of all servers
that libli is allowed to interact with, so libli can display the
content of their rooms.

If we want all servers (since matrix is decentralized, like libli.org
we could want a public global instance of our client), we can use
`["*"]` where the only matrix homeserver is `*` widcard value.

If we would like our libli client to be restricted to display the
content of rooms from only one server, we can set the value to
`["https://example.org"]` (the origin of our matrix homeserver,
`https://matrix.org` for example).

## hash

A matrix `room_alias` (for example`#libli:matrix.org` for this
project's space)

It will display this room/space as the homepage of the libli instance,
and pin it into the libli-menu (on the `/menu` page).

## hash-pin

A boolean value (as an HTML string attribute), if sets to `true` will
only allow displaying the matrix room_alias set in the `hash`
attribute.

> It currently does not allow rooms that are children or parents of a
> "hash pinned" space/room, but it would be cool.

## homeserver

Sets if this libli instance is bound to a matrix homeserver (as in, it
should "represent a specfic homeserver instance"). It then resolves
"clean URLs" (stripped from the `homeserver` part of the matrix
`room_alias`) for libli's URL entrypoint.

Set the value to the `origin` of a matrix homeserver; we can use
`https://matrix.org` or `https://example.org` if we have our
homeserver there (without the "pathname" part).

In these cases, the following URL would resolve the same room (because
we defined on which server it should by default look for them).

```
# both these URL would resolve the same rooms
https://example.org/my-custom-libli/#matrix-dev:matrix.org
https://example.org/my-custom-libli/matrix-dev

# also these
https://libli.org/#about-libli:matrix.org
https://libli.org/about-libli
https://libli.org/#about-libli

# it works with a # too, as libli-router currently works this way
```

> Take care of these features whem publishing a "closed libli client"
> as some content from other rooms my be read from URLs of your
> website. We might need to use other attributes to restrict from wich
> rooms/spaces and homeservers our (custom) matrix client can display
> content from.

### Notes

Could maybe be a boolean and the first value of
`homeserver-authorized`, or maybe also be multiple values; tbd.

## debug

Shows some debug info about the matrix events and the content of rooms
in the UI (from the HTML attributes). Can also be accessed by appeding
5 `#` to any libli URL (that does not already have a hash).

# Legacy attributes

Attributes that are deprecated or soon to be (renamed or removed).

## hostname (tbd)

Currently similar to `window.location.hostname`, but not used as "a
URL" thing, but more of a "app-name" attribute, or for `document.title`.

## homeserver

The matrix `homeserver`, replaced by `homeserver-authorized`.
