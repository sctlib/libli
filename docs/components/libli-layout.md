# libli-layout

> Does not accept attributes used in JS, used for its children in
> slots.

Has a shadow dom that accepts three slots `page`, `player` and `nav`,
with `parts` of the same name.

It also creates the `libli-layout-{part}` children HTML element.

This component is used by the libli-app to create to display each
elements composing the UI on the screen.
