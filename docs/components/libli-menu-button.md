# libli-menu-button

Inserts the `…` main menu of the app's interface.

When clicked:

- opens the `/menu` page/route, if not already on there
- if already on the menu page, goes back to the previous page
- otherwise goes back to the homepage

> It could be possible to render the menu without changing route in
> the browse URL. Currently not done as the plan is to progressivelly
> hide most of the interface, as we learn how use shortcuts and
> cutomize the interface.
