# libli-player

> Currently does not accept any attribute configuration, and only js
> methods. It keeps almost no state and provides minimal features
> (which should be enhanced as soon as its APIs mature).

Display a `matrix-room` component and currently a `radio4000-player`
media player, to play the content of a room ("as radio4000 playlist
tracks", which supports a few providers, Youtube, Soundcoud, Vimeo,
files ).

> The r4 player is lazy loaded from a CDN when events it supports are
> played; therefore it is not part of the package.json dependencies.

## Play a matrix event

Use the `playEvent(matrixEvent)` method, with a "matrix event"
paramater, as returned from the API of a homeserver (or mwc).

This method will use the `event_id` and `room_id` for this event, to
create a new `matrix-room`, that will list events of this type if the
player can play them.

It will also create a `radio4000-player` set to only show the media
(as provided by the media provider, ex: youtube iframe).

Currently the only features are to play the events of type
`org.libli.message.track` (a radio4000 track model).

At the end of a track (with a "media URL") it will play the next one (from the matrix
context API)

## Play the next/previous matrix event

The `playNextEvent(matrixEvent)` method and
`playpreviousEvent(matrixEvent)` use the "current matrix event" as
parameter, to find the previous to play (from the context API).

> These events are registered as custom events (with for and display
> template) in the imported mwc. See the
> `/src/templates/matrix-events` folder.

## Player UI

The `closePlayer()` and `minimizePlayer()` methods can be used to
change the UI of the player. Where close removes the player from the
DOM (a play event will "pop a new one in DOM" if none extist).

> Could in theory have multiples, but the UI does not yet allow it (or
> multiple libli.s in one page).

## Player service

See the `./services/player.js` file for an overview of how to register
a player, for a matrix event type (experiemental; also "display
events" for events sent by the mwc display templates, because re-used
"as the player's "tracklist").

### Player events

For each "registered custom matrix events", the player will listen to
`<custom_event_name>.eventEnded` DOM event, which `detail` value
should be the matrix event that ended (it needs the `event_id` key)

## Notes

Diverse considerations.

### Files and mxc URLS

Currently cannot upload a file (like in a `m.room.message` event) and
reference it as the media in the same event (with this mwc template,
but it should handle conversion of an attribute from an "input type
file" into a `mxc://` URL, which the player does not handle yet; tbd).
