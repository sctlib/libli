# libli-router

Uses https://github.com/visionmedia/page.js/ to handle the various
URLs and internal links in entire libli.

It uses most attributes of `libli-app` to decode the current browser
URL into a "matrix profile" (room id alias etc.) into a libli page (or
an "internal libli page", such as /auth or /menu etc.).

Maybe the router should be replaced with the browser URL pattern API
and then moved within mwc (currently not as it does not accept any
dependency).

- https://developer.mozilla.org/en-US/docs/Web/API/URLPattern#see_also
- https://github.com/kenchris/urlpattern-polyfill

Each time a page it knows matches, it renders it, as a new web
component for the registered page. It also sets a few attributes on
every pages.

The pages web-components are in the `/src/pages/` folder. The
`profile.js` page handles every matrix room display ("libli
profiles"), except the homepage.
