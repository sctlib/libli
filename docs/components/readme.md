# libli components

Each of the components are used to build the interface.

In addition the the `@sctlib/mwc` components (`matrix-room` etc.)
imported as dependency to libli.

The main component is `libli-app` which setups and lays-out the
others.
