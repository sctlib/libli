# Development

- all code is unminified HTML/CSS/JS (web-components)
- built tool is `vite` and `prettier` is used for linting

## Local server

Run dev server with:

```
npm run dev
```

Access locally at http://localhost:4040/, you will have to reresh the
page to see your changes appear.

## Deployment

To build the pages (site) and lib (libli.js), run (or see the `package.json`).

```
npm run build
# outputs dist-{pages,lib}
```

> Also customize the values of the `.env.json` file for production in the `/assets/` folder

## Dependencies

All dependencies are right now imported through a CDN,
https://www.jsdelivr.com, and linked in libli directly as imports.

> We might want to remove this dependency by doing manual imports in the HTML (tbdf).

### `DOMPurify === window.Sanitizer`

There is a "hard dependency" to https://github.com/cure53/DOMPurify. in the file `src/index.js`.

It is used as a polyfill to
https://developer.mozilla.org/en-US/docs/Web/API/Sanitizer, and should
be removed once this web API is available in browsers.

Used for :

- event of type message, `formatted_body`, when inserting user generated HTML to the DOM.
- libli widgets of type CSS style, when inserted to the dom in `<style>` tags.

### `page` for URL router

To decide what page/component to render, from the current URL address,
and get the URL parameters (such as the `room_id` and `event_id`).

- https://github.com/visionmedia/page.js
- https://www.npmjs.com/package/page

### `@sctlib/mwc`

All communication to the Matrix APIs is made by an other web component, `src/components/index.js`.

- https://gitlab.com/sctlib/mwc
- https://www.npmjs.com/package/@sctlib/mwc

# Building the site for production

To create a build output for production, use `npm run build` which
outputs the `dist-pages/` folder ready to be deployed.

We should not forget to customize the values of the `.env.json` file,
in case we use that on our site.

> The `.env*.json` file should **never contain any secret**
> variables. It is only used for public component configuration, and
> to manage the different project's environement specific
> configuration.

Also, in the case of gitlab pages, don't for get the
`/assets/_redirects` file, which should tell the host (gitlab pages)
to redirect all URL requests to the `index.html`, so our javascript
can decide what to do with the URL requested.

# Publishing site & package (libli)

Deployemnt & npm-publishing through CI/CD (gitlab), see the
`.gitlab-ci.yml` file.

## npm package

Every tagged version `v*` on branch main in published to npm.

Check npm or the gitlab tags to get the latest version.

- gitlab version tagged `v0.0.1`
- will publish a npm version `0.0.1`

## libli.org site

Hosted on https://sctlib.gitlab.io/libli and redirected to https://libli.org.

### deploy your own live version

To host your own version, try https://gitlab.com/sctlib/sctlib.org
