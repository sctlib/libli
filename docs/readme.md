# Usage

Install the npm package:

- `npm i --save @sctlib/libli`
- `import libli from "@sctlib/libli"`
- then use the `libli-app` custom element in an HTML, with a
  configuration via its attributes (more details in the
  `/docs/components/` folder, or example above).

Check the other files in this folder for more dedicated informations.

> There are no CSS styles bundled with libli, so it should be
> customized or the styles from this project can be copied (until more
> theming capabilities).

Otherwise, to get started with libli on your website, the quickest is
to import the npm package via a CDN.

```html
<script type="module" src="https://cdn.jsdelivr.net/npm/@sctlib/libli"></script>
```

Or with a "pinned version tag" (we should, othwerwise we might get
breaking changes from updats).

```html
<script
	type="module"
	src="https://cdn.jsdelivr.net/npm/@sctlib/libli@0.0.31"
></script>
```

To create a new libli-app in the HTML page, we can configure and use
this javascript snippet:

```html
<script>
	const $app = document.createElement("libli-app");
	$app.setAttribute("origin", window.location.origin);
	$app.setAttribute("pathname", window.location.pathname);
	$app.setAttribute("hostname", "libli");
	$app.setAttribute("homeserver-authorized", JSON.stringify(["*"]));
	$app.setAttribute("hash", "#libli:matrix.org");
	globalThis.document.querySelector("body").append($app);
</script>
```

Which would result in the following HTML element being inserted in the
page's body (if our libli client's HTML page is served from the
`https://example.org/libli/` URL address):

```html
<libli-app
	origin="https://example.org"
	pathname="/libli/"
	hostname="my-libli"
	homeserver-authorized='["*"]'
	hash="#my-room:example.org"
></libli-app>
```

> In this case we would display the room `#my-room:example.org` on the
> homepage. Note that the `homeserver-authorized` value is a JSON
> Array, so beware of `""` and `''` when writting the values manually.

# Development

For local development, see the dedicated file in this folder.
