# libli

Libli is a (light) [Matrix](https://matrix.org) client, for displaying
the content of public rooms and browse it through a player. It is also
possible to customize through "custom libli widgets". It is made with
web technologies, HTML, CSS web components, and distributed as a npm
package.

- [documentation](https://gitlab.com/sctlib/libli/-/tree/main/docs) folder for usage and more details
- https://gitlab.com/sctlib/libli
- https://www.npmjs.com/package/@sctlib/libli (comes without styles).
- [#libli:matrix.org](https://matrix.to/#/#libli:matrix.org) space for
  community chats and content
- https://libli.org/

Libli is using the npm module
[@sct/mwc](https://gitlab.com/sctlib/mwc) that communicates with the
matrix APIs (also using web-components).

When defined, and inserted in the DOM the `libli-app` web component, will display the content of public matrix rooms, taking the `room_alias` from the the current URL (`window.location`).

It works like so:

- `https://example.org/#room_alias:domain.tld`
- `https://libli.org/#libli:matrix.org`

Where:

- `room_alias` is the `alias` of the public matrix room
- `domain` the domain name of the Matrix server on which the room is created
- `.tld` is the Top Level Domain name of the server

This works for matrix rooms, `#room_alias:domain.tld` (not yet for
room IDs or users `@user_id:matrix.tld`).

# Example

Insert libli on any HTML page with this code (importing from a CDN), to create an instance of libli, that allows reading rooms from all homeservers (the `*` wildcard i `homeserver-authorized` value), and "pins" the `#libli:matrix.org` room on the homepage.

```html
<script type="module" src="https://cdn.jsdelivr.net/npm/@sctlib/libli"></script>
<script>
	const $app = document.createElement("libli-app");
	$app.setAttribute("origin", window.location.origin);
	$app.setAttribute("pathname", window.location.pathname);
	$app.setAttribute("hostname", "libli");
	$app.setAttribute("homeserver-authorized", JSON.stringify(["*"]));
	$app.setAttribute("hash", "#libli:matrix.org");
	globalThis.document.querySelector("body").append($app);
</script>
```

# License(s)

- Logo (gitlab, matrix room): all rights
  https://www.reddit.com/r/solarpunk/wiki/index/
- Code: (a)GPLv3 https://www.gnu.org/licenses/gpl-3.0.en.html
