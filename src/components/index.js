import roomWidgetsDefinitions from "./widgets/index.js";

import LibliPlayer from "./libli-player.js";
import LibliLayout from "./libli-layout.js";
import LibliRouter from "./libli-router.js";
import LibliApp from "./libli-app.js";
import LibliMenuButton from "./libli-menu-button.js";
import LibliMenu from "./libli-menu.js";
import LibliControls from "./libli-controls.js";
import MatrixSendTrack from "./matrix-send-track.js";

const componentDefinitions = {
	"libli-player": LibliPlayer,
	"libli-menu": LibliMenu,
	"libli-layout": LibliLayout,
	"libli-router": LibliRouter,
	"libli-app": LibliApp,
	"libli-menu": LibliMenu,
	"libli-menu-button": LibliMenuButton,
	"libli-controls": LibliControls,
	"matrix-send-track": MatrixSendTrack,
};

const allComponentDefinitions = {
	...componentDefinitions,
	...roomWidgetsDefinitions,
};

export { allComponentDefinitions as default };
