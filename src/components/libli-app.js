import mwc, {
	addDisplayListeners,
	removeDisplayListeners,
} from "../services/mwc.js";
import {
	getLibliWidgets,
	getClientWidgetParams,
} from "../services/libli-widgets.js";
const HASH_DEBUG_CODE = "#####";

export default class LibliApp extends HTMLElement {
	static get observedAttributes() {
		return [
			"homeserver",
			"homeserver-authorized",
			"origin",
			"hostname",
			"pathname",
			"hash",
			"hash-pin",
			"debug",
		];
	}
	get homeserver() {
		return this.getAttribute("homeserver") || "";
	}
	get homeserverAuthorized() {
		return this.getAttribute("homeserver-authorized") || "";
	}
	get origin() {
		return this.getAttribute("origin") || "";
	}
	get hostname() {
		return this.getAttribute("hostname") || "";
	}
	get pathname() {
		return this.getAttribute("pathname") || "";
	}
	get hash() {
		return this.getAttribute("hash") || "";
	}
	/* can be set when embeded as a widget in a matrix client */
	set hash(profileId) {
		if (profileId) {
			this.setAttribute("hash", profileId);
		} else {
			this.removetAttribute("hash");
		}
	}
	get hashPin() {
		return this.getAttribute("hash-pin") || "";
	}
	get debug() {
		return this.getAttribute("debug");
	}
	set debug(str) {
		return this.setAttribute("debug", str);
	}

	/* state */
	get $layout() {
		return this.querySelector("libli-layout");
	}
	get $player() {
		return this.querySelector("libli-player");
	}

	/* public methods */
	playEvent(matrixEvent) {
		/* console.info("app.playEvent", matrixEvent); */
		if (!this.$player) {
			this.popPlayer();
		}
		this.$player.playEvent(matrixEvent);
	}

	/* lifecycle */
	connectedCallback() {
		if (window.location.hash === HASH_DEBUG_CODE) {
			this.debug = true;
		}

		/* if libli is embeded in a matrix client as a widget,
			 we set it up "minimally", by prefilling some of its  attributes */
		this.clientWidgetParams = getClientWidgetParams(window.location.href);
		if (
			this.clientWidgetParams?.matrix_room_id &&
			this.clientWidgetParams.matrix_widget_id
		) {
			this.hash = this.clientWidgetParams.matrix_room_id;
			console.info(
				"Libli is a matrix client widget",
				this.clientWidgetParams.clientWidgetParams,
			);
		} else {
			console.info("Libli is standalone, not a matrix client widget");
		}
		addDisplayListeners(this);
		this.render();
	}
	disconnectedCallback() {
		removeDisplayListeners(this);
	}

	render() {
		this.replaceChildren();
		const $layout = document.createElement("libli-layout");
		$layout.append(...this.createNavs());
		$layout.append(this.createRouter());
		this.replaceChildren($layout);
	}
	createRouter() {
		const $r = document.createElement("libli-router");
		$r.setAttribute("homeserver", this.homeserver);
		$r.setAttribute("homeserver-authorized", this.homeserverAuthorized);
		$r.setAttribute("origin", this.origin);
		$r.setAttribute("hostname", this.hostname);
		$r.setAttribute("pathname", this.pathname);
		$r.setAttribute("hash", this.hash);
		$r.setAttribute("hash-pin", this.hashPin);
		$r.setAttribute("slot", "page");
		return $r;
	}
	createNavs() {
		const $controls = document.createElement("libli-controls");
		$controls.setAttribute("slot", "nav");
		$controls.setAttribute("origin", this.origin);

		const $authMenu = document.createElement("libli-menu-button");
		$authMenu.setAttribute("url", "/menu");
		$authMenu.setAttribute("slot", "nav");

		return [$controls, $authMenu];
	}
	createPlayer() {
		const $player = document.createElement("libli-player");
		$player.setAttribute("origin", this.origin);
		$player.setAttribute("slot", "player");
		return $player;
	}

	/* pop is a render after initial render; could also be ouside app? */
	popPlayer() {
		const $newPlayer = this.createPlayer();
		if (this.$player) {
			this.$layout.replaceWith($newPlayer, this.$player);
		} else {
			this.$layout.append($newPlayer);
		}
	}
}
