import mwc from "../services/mwc.js";
import { page } from "../services/router.js";

export default class LibliControls extends HTMLElement {
	/* props */
	get origin() {
		return this.getAttribute("origin");
	}
	/* dom utils */
	get $dialog() {
		return this.querySelector("wcu-dialog");
	}
	get $search() {
		return this.querySelector("matrix-search");
	}
	/* methods */
	openDialog() {
		this.$dialog.open();
	}
	closeDialog() {
		this.$dialog.$dialog.close();
	}
	onSubmitSearch(event) {
		/* this.openDialog(); */
		event.preventDefault();
	}
	onSubmitForm(event) {
		event.preventDefault();
		/* otherwise the form seems to close it */
		this.openDialog();
	}
	async onInput({ target: { value, name } }) {
		try {
			const profile = await mwc.api.checkMatrixId(value);
			const { profileId } = profile;
			if (profileId) {
				if (this.$search.data.has(profileId)) {
					page(`/${profileId}`);
					this.closeDialog();
				}
			}
		} catch (e) {
			console.log("Error getting libli-controls joined rooms", e);
		}
	}
	async onAuth({ detail }) {
		this.render();
	}
	onDialogOpen(event) {
		event.preventDefault();
		this.$search.focus();
	}
	onDialogClose(event) {
		console.log("dialog closed", event);
	}
	onButtonOpen(event) {
		event.preventDefault();
		if (this.$dialog.$dialog.open) {
			this.closeDialog();
		} else {
			this.openDialog();
		}
	}
	async connectedCallback() {
		/* this.syncAbortController = new AbortController();
			 const sync = await mwc.api.sync({
			 signal: this.syncAbortController.signal,
			 });
			 console.log("sync", sync); */
		mwc.api.addEventListener("auth", this.onAuth.bind(this));
		this.render();
	}
	disconnectedCallback() {
		mwc.api.removeEventListener("auth", this.onAuth);
	}
	render() {
		const $form = this.createForm();
		const $dialog = document.createElement("wcu-dialog");
		$dialog.addEventListener("open", this.onDialogOpen.bind(this));
		$dialog.addEventListener("close", this.onDialogClose.bind(this));
		const $buttonOpen = document.createElement("button");
		$buttonOpen.addEventListener("click", this.onButtonOpen.bind(this));
		$buttonOpen.setAttribute("slot", "open");
		$buttonOpen.setAttribute("title", "Search controls (accesskey: k)");
		$buttonOpen.setAttribute("accesskey", "k");
		$buttonOpen.textContent = "⌕";
		$form.setAttribute("slot", "dialog");
		$dialog.append($buttonOpen, $form);
		this.replaceChildren($dialog);
	}
	createForm() {
		const $search = document.createElement("matrix-search");
		$search.addEventListener("input", this.onInput.bind(this));
		$search.addEventListener("mxsearch", this.onSubmitSearch.bind(this));
		$search.addEventListener("submit", this.onSubmitForm.bind(this));
		this.origin && $search.setAttribute("origin", this.origin);
		return $search;
	}
}
