export default class LibliLayout extends HTMLElement {
	/* dom state query selector */
	get $page() {
		return this.querySelector('slot[name="page"]');
	}
	get $player() {
		return this.querySelector('slot[name="player"]');
	}
	get $nav() {
		return this.querySelector('slot[name="nav"]');
	}
	connectedCallback() {
		this.render();
	}
	render() {
		this.shadow = this.attachShadow({ mode: "open" });
		const $layouts = [
			this.createSlot("page"),
			this.createSlot("player"),
			this.createSlot("nav"),
		];
		const $root = document.createElement(`libli-layout-root`);
		$root.setAttribute("part", "root");
		$root.append(...$layouts);
		this.shadow.replaceChildren($root);
	}
	createSlot(name) {
		const $l = document.createElement(`libli-layout-${name}`);
		$l.setAttribute("part", name);
		const $s = document.createElement("slot");
		$s.setAttribute("name", name);
		$l.append($s);
		return $l;
	}
}
