import { page } from "../services/router.js";

export default class LibliMenuButton extends HTMLElement {
	static get observedAttributes() {
		return ["url", "is-active"];
	}
	get url() {
		let menuUrl = this.getAttribute("url");
		if (menuUrl.startsWith("/")) {
			return menuUrl.slice(1);
		} else {
			return menuUrl;
		}
	}
	get isActive() {
		const pathParts = window.location.pathname.split("/");
		return pathParts[pathParts.length - 1] === this.url;
	}
	connectedCallback() {
		this.render();
	}
	attributeChangedCallback(attrName, oldValue, newValue) {
		if (attrName === "url") {
			this.render();
		}
	}
	render() {
		this.innerHTML = "";
		const $menuLink = document.createElement("button");
		$menuLink.addEventListener("click", this.handleClick.bind(this));
		$menuLink.setAttribute("title", "Menu (accesskey: m)");
		$menuLink.setAttribute("accesskey", "m");
		$menuLink.textContent = "...";
		this.append($menuLink);
	}
	handleClick(event) {
		if (this.isActive) {
			if (history.length <= 2) {
				page(`/`);
			} else {
				history.go(-1);
			}
		} else {
			page(`/${this.url}`);
		}
	}
}
