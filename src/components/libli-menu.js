const LINKS = [
	{
		path: "/",
		text: "home",
		title: "Homepage of this site",
	},
	{
		path: "widgets",
		text: "widgets",
		title: "Widgets customization",
	},
	{
		path: "auth",
		text: "auth",
		title: "User authentication",
	},
];

export default class LibliMenu extends HTMLElement {
	static get observedAttributes() {
		return ["origin", "hostname", "hash"];
	}
	get origin() {
		return this.getAttribute("origin");
	}
	get hostname() {
		return this.getAttribute("hostname");
	}
	get hash() {
		return this.getAttribute("hash");
	}
	connectedCallback() {
		this.render();
	}
	render() {
		const $allLinks = [];
		const $links = LINKS.map(({ text, path, title }) => {
			const $link = document.createElement("a");
			$link.setAttribute("title", title);
			if (path === "/") {
				$link.setAttribute("href", this.origin);
				$link.textContent = this.hostname || text;
			} else {
				$link.setAttribute("href", `${this.origin}/${path}`);
				$link.textContent = text;
			}
			return $link;
		});

		$allLinks.push(...$links);
		if (this.hash) {
			const $hashLink = document.createElement("a");
			$hashLink.setAttribute("href", `${this.origin}/${this.hash}`);
			$hashLink.setAttribute("title", "Site content");
			$hashLink.textContent = this.hash;
			$allLinks.push($hashLink);
		}
		const $listItems = $allLinks.map(($link) => {
			const $li = document.createElement("li");
			$li.append($link);
			return $li;
		});

		const $menu = document.createElement("menu");
		$menu.append(...$listItems);
		this.replaceChildren($menu);
	}
}
