import mwc, {
	addDisplayListeners,
	removeDisplayListeners,
} from "../services/mwc.js";
import playerService, { PLAYER_EVENTS } from "../services/player.js";

const { TRACK_ENDED } = PLAYER_EVENTS;

export default class LibliPlayer extends HTMLElement {
	static get observedAttributes() {
		return ["origin", "event-id", "room-id"];
	}
	/* props */
	get origin() {
		return this.getAttribute("origin");
	}
	get eventId() {
		return this.getAttribute("event-id");
	}
	get roomId() {
		return this.getAttribute("room-id");
	}

	/* state */
	get $layout() {
		return this.querySelector("libli-player-layout");
	}
	get $details() {
		return this.querySelector("details");
	}
	get $room() {
		return this.querySelector("matrix-room");
	}

	/* public methods */
	async playEvent(matrixEvent) {
		/* console.info("player.playEvent", matrixEvent); */
		if (matrixEvent) {
			await this.updateRoom(matrixEvent);
			await this.updatePlayer(matrixEvent);
		}
	}
	async playNextEvent(matrixEvent) {
		const { events_before } = await mwc.api.getRoomEventContext({
			roomId: matrixEvent.room_id,
			eventId: matrixEvent.event_id,
			params: [
				[
					"filter",
					{
						types: [matrixEvent.type],
					},
				],
			],
		});
		if (events_before?.length) {
			const nextMatrixEvent = events_before.find(
				({ content, redacted_because, type }) => {
					return content && !redacted_because & (type === matrixEvent.type);
				},
			);
			if (nextMatrixEvent) {
				this.playEvent(nextMatrixEvent);
			} else {
			}
		}
	}
	async playPreviousEvent(matrixEvent) {
		const { events_after } = await mwc.api.getRoomEventContext({
			roomId: matrixEvent.room_id,
			eventId: matrixEvent.event_id,
			params: [
				[
					"filter",
					{
						types: [matrixEvent.type],
					},
				],
			],
		});
		if (events_after?.length) {
			const previousMatrixEvent = events_after[0];
			this.playEvent(previousMatrixEvent);
		}
	}

	/* private methods */
	closePlayer() {
		this.remove();
	}
	minimizePlayer() {
		this.$details.toggleAttribute("open");
	}
	domEventPlayer(player) {
		if (!player) return;
		const $tempPlayer = player.createElement();
		const $existingPlayer = this.querySelector(
			$tempPlayer.tagName.toLowerCase(),
		);
		return $existingPlayer;
	}
	async updateRoom(matrixEvent) {
		if (!this.$room || this.$room.getAttribute("is-error") === "true") {
			await this.asyncRenderRoom(matrixEvent);
		} else if (this.$room.getAttribute("profile-id") !== matrixEvent.room_id) {
			await this.asyncRenderRoom(matrixEvent);
		}
	}
	async updatePlayer(matrixEvent) {
		const { type } = matrixEvent;
		const eventPlayer = playerService.players.get(type);
		if (!eventPlayer) {
			return;
		}
		if (!this.domEventPlayer(eventPlayer)) {
			try {
				await eventPlayer.load(this, matrixEvent);
				if (!this.domEventPlayer(eventPlayer)) {
					const $newPlayer = await eventPlayer.createElement(matrixEvent);
					this.$layout.append($newPlayer);
				} else {
				}
			} catch (e) {}
		}

		if (this.domEventPlayer(eventPlayer)) {
			/* we remove it on event ended;
			 @TODO: should remove on */
			this.addEventListener(
				`${matrixEvent.type}.${TRACK_ENDED}`,
				this.onEventEnded,
			);
			eventPlayer.playEvent(matrixEvent, this.domEventPlayer(eventPlayer));
		}
	}

	/* ui events */
	async onEventEnded(playerEvent) {
		const matrixEventType = playerEvent.type.split(`.${TRACK_ENDED}`)[0];

		this.removeEventListener(
			`${matrixEventType.type}.eventEnded`,
			this.onEventEnded,
		);

		const player = playerService.players.get(matrixEventType);
		const eventId = player.onEventEnded(playerEvent.detail);
		const roomId = this.$room.getAttribute("profile-id");
		const matrixEvent = await mwc.api.getEvent({
			roomId,
			eventId,
		});
		this.playNextEvent(matrixEvent);
	}

	/* lifecycle */
	constructor() {
		super();
		addDisplayListeners(this, {
			stopPropagation: true,
		});
	}
	connectedCallback() {
		this.renderLayout();
	}
	disconnectedCallback() {
		removeDisplayListeners(this);
	}

	/* sync render layout  */
	renderLayout() {
		const $layout = this.createLayout();
		this.replaceChildren($layout);
	}
	/* async render */
	asyncRenderPlayer(player, matrixEvent) {
		return new Promise((resolve) => {
			const $player = this.createPlayerForEvent(matrixEvent, resolve);
			/* const $player = player.createElement(matrixEvent); */
			this.$layout.append($player);
		});
	}
	asyncRenderRoom(matrixEvent) {
		return new Promise((resolve) => {
			const $room = this.createRoomForEvent(matrixEvent, resolve);
			if (!this.$room) {
				this.$layout.append($room);
			} else {
				this.$room.replaceWith($room);
			}
		});
	}
	createRoomForEvent(matrixEvent, roomReadyResolve) {
		const { room_id, event_id } = matrixEvent;
		const $room = document.createElement("matrix-room");
		$room.addEventListener("mxready", roomReadyResolve);
		$room.setAttribute("show-context", true);
		$room.setAttribute("profile-id", room_id);
		$room.setAttribute("event-id", event_id);
		$room.setAttribute("slot", "page");
		this.origin && $room.setAttribute("origin", this.origin);
		return $room;
	}
	createPlayerActions() {
		const $summary = document.createElement("summary");

		const $close = document.createElement("button");
		$close.addEventListener("click", this.closePlayer.bind(this));
		$close.innerText = "x";

		const $mini = document.createElement("button");
		$mini.addEventListener("click", this.minimizePlayer.bind(this));
		$mini.setAttribute("title", "Toggle player");
		$mini.innerText = "↨";
		$summary.append($close, $mini);
		return $summary;
	}
	createLayout() {
		const $playerLayout = document.createElement("libli-player-layout");
		const $playerDetails = document.createElement("details");
		$playerDetails.setAttribute("open", true);
		const $playerSummaryActions = this.createPlayerActions();
		$playerDetails.append($playerSummaryActions, $playerLayout);
		return $playerDetails;
	}
}
