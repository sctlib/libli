import { page, getHashPath } from "../services/router.js";
import { MATRIX_WIDGET_SEARCH_PARAMS } from "../services/libli-widgets.js";

const ROUTES = [
	{ path: "/", name: "home" },
	{
		path: "/widgets",
		name: "widgets",
		searchParams: [
			/* from libli URL */
			"config",
			"type",
			...MATRIX_WIDGET_SEARCH_PARAMS,
		],
	},
	{
		path: "/widgets/:widget_type",
		name: "widgets",
		searchParams: ["config", "type"],
		params: ["widget_type"],
	},
	{ path: "/auth", name: "auth" },
	{ path: "/menu", name: "menu" },
	{
		path: "/:profile_id/:event_id?",
		name: "profile",
		params: ["profile_id", "event_id"],
	},
	{
		path: "/:profile_id/:event_id",
		name: "profile",
		params: ["profile_id", "event_id"],
	},
];

export default class LibliRouter extends HTMLElement {
	static get observedAttributes() {
		return [
			"homeserver",
			"homeserver-authorized",
			"origin",
			"hostname",
			"pathname",
			"hash",
			"hash-pin",
		];
	}
	/* is this instance of libli, bound to an underlying server?
		 it will clean URLs for this domain, so it looks like pages */
	get homeserver() {
		return this.getAttribute("homeserver") || "";
	}
	get homeserverAuthorized() {
		const servers = this.getAttribute("homeserver-authorized");
		return servers ? JSON.parse(servers) : [];
	}
	get origin() {
		return this.getAttribute("origin") || "";
	}
	set origin(str) {
		/* can be (re)set, if we're on domain translate.goog (for example),
			 so links work, when the libli client is embeded somewhere */
		return this.setAttribute("origin", str);
	}
	get hostname() {
		return this.getAttribute("hostname") || "";
	}
	/* used by the router page.js, 'expample.org/pathname/' */
	get pathname() {
		return this.getAttribute("pathname") || "";
	}
	/* used for the page js router */
	set pathname(str) {
		this.setAttribute("pathname", str);
	}
	/* the index profile, displayed on the pathname (base url of the platform) */
	get hash() {
		return this.getAttribute("hash") || "";
	}
	/* are we "only display" the hash profile?,
		 always true (if there is a hash), except if set to "false" */
	get hashPin() {
		return this.getAttribute("hash-pin") === "true";
	}

	/* state */
	get searchParams() {
		return new URLSearchParams(window.location.search);
	}
	/* methods */
	/* general set of the page's title */
	documentTitleMW(ctx, next) {
		document.title = this.hostname || document.title || "";
		next();
	}

	/* events */
	onHashChange({ hash, pathname }) {
		const path = getHashPath({
			pathname,
			hash,
		});
		page.redirect(path);
	}

	/* the route pages handlers */
	setupRoutes({
		pathname,
		origin,
		homeserver,
		homeserverAuthorized,
		hash,
		hostname,
	}) {
		/* the base URL of the router */
		if (pathname.length > 1) {
			page.base(pathname);
		}

		page("*", this.documentTitleMW.bind(this), (ctx, next) => {
			const { hash } = ctx;
			if (hash) {
				this.onHashChange({
					pathname: this.pathname,
					hash,
				});
			}
			this.replaceChildren();
			this.focus();
			next();
		});

		ROUTES.forEach((route) => {
			page(route.path, (ctx, next) => {
				const $page = this.createPage({ route, ctx });
				this.replaceChildren($page);
				this.focus();
				/* no next because we only want the first route to catch (last middleware?) */
				/* next(); */
			});
		});
	}

	searchParamToDomAttribute(serachParamName) {
		return serachParamName.replaceAll("_", "-");
	}

	/* lifecycle */
	constructor() {
		super();
		/* window.addEventListener("hashchange", this.onHashChange.bind(this)); */
	}
	connectedCallback() {
		this.setAttribute("tabindex", -1);
		this.pathname = this.pathname || "/";

		/* the routes handle the render */
		this.setupRoutes({
			origin: this.origin,
			hostname: this.hostname,
			pathname: this.pathname,
			homeserver: this.homeserver,
			homeserverAuthorized: this.homeserverAuthorized,
			hash: this.hash,
			hashPin: this.hashPin,
		});

		/* initial call to handle current url */
		page();
	}

	createPage({ route, ctx }) {
		const $page = document.createElement(`page-${route.name}`);
		$page.setAttribute("hostname", this.hostname);
		$page.setAttribute("homeserver", this.homeserver);
		$page.setAttribute("homeserver-authorized", this.homeserverAuthorized);
		$page.setAttribute("origin", this.origin);
		$page.setAttribute("hostname", this.hostname);
		$page.setAttribute("pathname", this.pathname);
		$page.setAttribute("hash", this.hash);
		$page.setAttribute("hash-pin", this.hashPin);

		/* se the URLSearchParams key/val on the page */
		if (route.searchParams && this.searchParams) {
			route.searchParams.forEach((param) => {
				const paramVal = this.searchParams.get(param);
				const attrName = this.searchParamToDomAttribute(param);
				paramVal && $page.setAttribute(attrName, paramVal);
			});
		}

		/* set the route params and their value on the page */
		if (route.params) {
			route.params.forEach((param) => {
				const attrName = this.searchParamToDomAttribute(param);
				const val = ctx.params[param];
				if (val) {
					$page.setAttribute(attrName, val);
				}
			});
		}
		return $page;
	}
}
