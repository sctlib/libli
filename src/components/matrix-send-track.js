export const formTemplate = document.createElement("template");
formTemplate.innerHTML = `
	<form>
		<fieldset>
			<legend>URL (youtube, soundclound, vimeo, file url)</legend>
			<input
						name="url"
						type="url"
						placeholder="https://{youtube,soundscloud,vimeo}.com/watch?v=1234abc"
						required="true"
			/>
		</fieldset>
		<fieldset>
			<legend>Discogs release URL</legend>
			<input
						name="discogs_url"
						type="url"
						placeholder="https://www.discogs.com/release/1234-abcd"
			/>
		</fieldset>
		<fieldset>
			<legend>Title</legend>
			<input
						name="title"
						placeholder="A small text used for title and name of this track"
						required="true"
			/>
		</fieldset>
		<fieldset>
			<legend>Description</legend>
			<textarea
							 name="body"
							 placeholder="A text descriptive of the media with #hashtags and @mentions"
				></textarea>
		</fieldset>
		<fieldset>
			<button type="submit">Create track</button>
		</fieldset>
	</form>
`;

export const PROVIDERS = {
	"www.youtube.com": "youtube",
	"youtube.com": "youtube",
	"youtu.be": "youtube",
	"soundcloud.com": "soundcloud",
	"vimeo.com": "vimeo",
};

const OEMBED_PROVIDERS = {
	soundcloud: "https://soundcloud.com/oembed?format=json&url=",
	youtube: "https://www.youtube.com/oembed?url=",
	vimeo: "https://vimeo.com/api/oembed.json?url=",
};

const getProvider = (hostname) => {
	return PROVIDERS[hostname];
};
const getProviderOEmbedUrl = (provider, urlText) => {
	return `${OEMBED_PROVIDERS[provider]}${urlText}`;
};

const getOEmbedUrl = (urlText) => {
	try {
		const url = new URL(urlText);
		const provider = getProvider(url.hostname);
		if (provider) {
			return getProviderOEmbedUrl(provider, urlText);
		}
	} catch (e) {
		// no data
	}
};

export default class MatrixSendTrack extends HTMLElement {
	/* state */
	oEmbedData = null;

	/* methods */
	fetchOEmbed(mediaProviderUrl) {
		const oEmbedUrl = getOEmbedUrl(mediaProviderUrl);
		if (oEmbedUrl) {
			return fetch(oEmbedUrl).then((res) => res.json());
		}
	}

	/* dom helps */
	get $formUrl() {
		return this.querySelector('input[name="url"]');
	}

	get $formTitle() {
		return this.querySelector('input[name="title"]');
	}

	setFormTitle(text) {
		if (this.$formTitle.value) {
			return;
		} else {
			this.$formTitle.value = text;
		}
	}
	async handleUrl(value) {
		if (value) {
			try {
				this.oEmbedData = await this.fetchOEmbed(value);
				if (this.oEmbedData?.title) {
					this.setFormTitle(this.oEmbedData.title);
				}
			} catch (error) {
				console.info("Error fetching oembed", error);
			}
		}
	}

	onUrlInput(event) {
		const { value, name } = event.target;
		if (name === "url") {
			this.handleUrl(value);
		} else {
			// no thing yet for other inputs
		}
	}
	connectedCallback() {
		this.render();
		this.$formUrl.addEventListener("input", this.onUrlInput.bind(this));
	}
	render() {
		const $form = formTemplate.content.cloneNode(true);
		this.replaceChildren($form);
	}
}
