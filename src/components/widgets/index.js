import LibliWidgetStyles from "./styles.js";
import LibliWidgetProfile from "./profile.js";

const componentDefinitions = {
	"libli-widget-styles": LibliWidgetStyles,
	"libli-widget-profile": LibliWidgetProfile,
};

export { componentDefinitions as default };
