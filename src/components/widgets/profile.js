import sanitize, { canSanitize } from "../../services/sanitizer.js";
import { WIDGET_CONTENT_KEY } from "../../services/libli-widgets.js";

const PROFILE_TYPES_MAP = {
	default: "(same with or without) a log, chat profile",
	images: "For listing and displaying images",
	journal: "For writting blog post like content",
	page: "For a HTML webpage, raw feeling",
	media: "To support sending track messages events to be played in the player",
};

const PROFILE_TYPES = Object.keys(PROFILE_TYPES_MAP);

export default class LibliWidgetProfile extends HTMLElement {
	static get observedAttributes() {
		return ["content"];
	}
	get content() {
		return this.getAttribute("content");
	}
	connectedCallback() {
		this.render();
	}
	render() {
		const $form = this.createForm(this.content);
		this.replaceChildren($form);
		if (!this.content) {
			const $info = this.createInfo();
			this.append($info);
		}
	}
	createForm(content) {
		const selectedType = content;
		const $form = document.createElement("form");

		const $selectFieldset = document.createElement("fieldset");
		const $selectLegend = document.createElement("legend");
		$selectLegend.textContent = "Libli (room) profile type";

		const $select = document.createElement("select");
		$select.setAttribute("name", WIDGET_CONTENT_KEY);
		const $options = PROFILE_TYPES.map((profileType) => {
			const $option = document.createElement("option");
			$option.textContent = profileType;
			$option.value = profileType;
			if (selectedType && selectedType === profileType) {
				$option.selected = true;
			}
			return $option;
		});
		$select.append(...$options);
		if (this.content) {
			$select.setAttribute("disabled", true);
		}
		$selectFieldset.append($selectLegend, $select);

		const $buttonFieldset = document.createElement("fieldset");
		const $button = document.createElement("button");
		$button.innerText = "Copy widget URL";
		$buttonFieldset.append($button);

		$form.append($selectFieldset);
		if (!this.content) {
			$form.append($buttonFieldset);
		}
		return $form;
	}
	createInfo() {
		const $list = document.createElement("ul");
		const $lis = PROFILE_TYPES.map((profileType) => {
			const $li = document.createElement("li");
			const $def = this.createProfileDefinition(profileType);
			$li.append($def);
			return $li;
		});
		$list.append(...$lis);
		return $list;
	}
	createProfileDefinition(profileType) {
		const $dl = document.createElement("dl");
		const $dt = document.createElement("dt");
		$dt.textContent = profileType;
		const $dd = document.createElement("dd");
		$dd.textContent = PROFILE_TYPES_MAP[profileType];
		$dl.append($dt, $dd);
		return $dl;
	}
}
