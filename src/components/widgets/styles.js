import { WIDGET_CONTENT_KEY } from "../../services/libli-widgets.js";

export default class LibliWidgetStyles extends HTMLElement {
	static get observedAttributes() {
		return ["content"];
	}
	get content() {
		return this.getAttribute("content");
	}
	connectedCallback() {
		this.render();
	}
	render() {
		const $dom = this.createDom(this.content);
		this.replaceChildren($dom);
	}
	createDom(content) {
		const $form = document.createElement("form");

		const $inputFieldset = document.createElement("fieldset");
		const $inputLegend = document.createElement("legend");
		$inputLegend.innerText = "CSS to customize a profile page (matrix room)";
		const $inputTextarea = document.createElement("textarea");
		$inputTextarea.setAttribute("name", WIDGET_CONTENT_KEY);
		if (content) {
			$inputTextarea.value = content;
			$inputTextarea.setAttribute("disabled", true);
		}
		$inputFieldset.append($inputLegend, $inputTextarea);

		const $submitFieldset = document.createElement("fieldset");
		const $submitLegend = document.createElement("legend");
		$submitLegend.textContent = "Copy the resulting widget URL";
		const $submitButton = document.createElement("button");
		$submitButton.setAttribute("type", "submit");
		$submitButton.textContent = "Copy widget URL";
		$submitFieldset.append($submitLegend);
		if (!this.content) {
			$submitFieldset.append($submitButton);
		}

		$form.append($inputFieldset, $submitFieldset);
		return $form;
	}
}
