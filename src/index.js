/* (before all) define the components manually, to register custom events */
window.MWC_MANUAL_DEFINE = true;
import mwc from "./services/mwc.js";

/* a user content sanitizer service */
import "./services/sanitizer.js";

import { defineComponents } from "./utils/components.js";
import componentDefinitions from "./components/index.js";
import pageDefinitions from "./pages/index.js";

const allComponentDefinitions = {
	...componentDefinitions,
	...pageDefinitions,
};

if (!window.LIBLI_MANUAL_DEFINE === true) {
	defineComponents(allComponentDefinitions);
}

export { allComponentDefinitions as default, mwc };
