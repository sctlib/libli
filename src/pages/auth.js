import { page } from "../services/router.js";

export default class PageAuth extends HTMLElement {
	/* lifecycle */
	connectedCallback() {
		const $dom = this.createDom();
		this.replaceChildren($dom);
	}
	createDom() {
		const $auth = document.createElement("matrix-auth");
		$auth.setAttribute("show-user", true);
		$auth.setAttribute("show-guest", true);
		return $auth;
	}
}
