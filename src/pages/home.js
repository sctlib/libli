import { canSanitize } from "../services/sanitizer.js";
import exampleRooms from "../utils/example-rooms.js";

export default class PageAbout extends HTMLElement {
	static get observedAttributes() {
		return ["origin", "hostname", "pathname", "hash"];
	}
	get origin() {
		return this.getAttribute("origin");
	}
	get hostname() {
		return this.getAttribute("hostname");
	}
	get pathname() {
		return this.getAttribute("pathname");
	}
	get profileId() {
		return this.getAttribute("hash") || exampleRooms.home;
	}
	connectedCallback() {
		if (this.profileId) {
			this.render();
		}
	}
	render() {
		const $room = document.createElement("matrix-room");
		$room.setAttribute("origin", this.origin);
		$room.setAttribute("profile-id", this.profileId);
		if (canSanitize) {
			$room.setAttribute("flag-sanitizer", true);
		}
		this.replaceChildren($room);
	}
}
