import PageHome from "./home.js";
import PageProfile from "./profile.js";
import PageWidgets from "./widgets.js";
import PageAuth from "./auth.js";
import PageMenu from "./menu.js";

const componentDefinitions = {
	"page-home": PageHome,
	"page-profile": PageProfile,
	"page-widgets": PageWidgets,
	"page-auth": PageAuth,
	"page-menu": PageMenu,
};

export default componentDefinitions;
