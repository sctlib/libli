export default class PageMenu extends HTMLElement {
	static get observedAttributes() {
		return ["origin", "hostname", "hash"];
	}
	get origin() {
		return this.getAttribute("origin");
	}
	get hostname() {
		return this.getAttribute("hostname");
	}
	get hash() {
		return this.getAttribute("hash");
	}
	connectedCallback() {
		this.render();
	}
	render() {
		const $menu = document.createElement("libli-menu");
		$menu.setAttribute("origin", this.origin);
		$menu.setAttribute("hostname", this.hostname);
		$menu.setAttribute("hash", this.hash);

		const $joinedRooms = document.createElement("matrix-joined-rooms");
		$joinedRooms.setAttribute("origin", this.origin);

		this.replaceChildren($menu, $joinedRooms);
	}
}
