import { canSanitize } from "../services/sanitizer.js";
import { page } from "../services/router.js";
import { getLibliWidgets } from "../services/libli-widgets.js";

export default class PageProfile extends HTMLElement {
	static get observedAttributes() {
		return ["homeserver", "origin", "profile-id", "event-id", "profile-type"];
	}
	/* props attr */
	get homeserver() {
		return this.getAttribute("homeserver");
	}
	get origin() {
		return this.getAttribute("origin");
	}
	get eventId() {
		return this.getAttribute("event-id");
	}
	get profileId() {
		return this.getAttribute("profile-id");
	}

	/* state*/
	get profileType() {
		return this.getAttribute("profile-type");
	}
	set profileType(str) {
		this.setAttribute("profile-type", str);
	}
	get context() {
		const searchParams = new URLSearchParams(window.location.search);
		return searchParams.get("context");
	}
	set context(event_id) {
		const searchParams = new URLSearchParams(window.location.search);
		searchParams.set("context", event_id);
		const newRelativePathQuery =
			window.location.pathname + "?" + searchParams.toString();
		history.pushState(null, "", newRelativePathQuery);
	}

	get profileFull() {
		let id = this.profileId;
		if (!id) return id;
		const host = id.split(":").length === 2;
		if (!host && this.homeserver) {
			id = `${id}:${new URL(this.homeserver).hostname}`;
		}
		if (["#", "@", "!"].includes(id.charAt(0))) {
			return id;
		} else {
			return `#${id}`;
		}
	}

	/* events handler */
	onEventSelect({ detail }) {
		const { event_id } = detail.mxevent;

		/* assign the context */
		this.context = event_id;

		/* skip the rest for now */
		if (true) return;

		/* go to "event detail page" */
		preventDefault();
		try {
			if (!this.eventId) {
				page(`/${this.profile}/${event_id}`);
			}
			/* when embeded in an other app, sometimes refused */
		} catch (error) {
			page.redirect(`/${this.profile}/${event_id}`);
		}
	}

	/* matrix room element is loaded */
	async onMatrixReady({ detail }) {
		if (!detail) return;

		const { events, state, widgets } = detail || {};

		/* if the ready events are not for this room (=== id or alias),
			 then is not for the room, but its space children/parent; skip them */
		if (!state || [state.alias, state.id].indexOf(this.profileFull) < 0) {
			return;
		}

		/* set the page title when we have it */
		document.title = state.name || this.profileFull;

		/* load widgets we know how to use */
		if (widgets && widgets.length) {
			this.handleMatrixWidgets(widgets);
		}
	}
	onPlayerStop({ detail = {} } = {}) {
		const { event, isTop } = detail;
		page(`/${this.profile}`);
		if (isTop && event) {
			const $event = this.$root.querySelector(
				`matrix-event[event-id="${event}"]`,
			);
			$event && $event.scrollIntoView();
		}
	}
	onContext({ detail }) {
		if (detail.event) {
			this.context = detail.event.event_id;
		}
	}
	handleMatrixWidgets(matrixWidgets) {
		const widgets = getLibliWidgets(matrixWidgets, this.origin);
		if (!widgets?.length) {
			return;
		} else {
			console.info("Registered libli widgets", widgets);
		}
		const profileWidget = widgets.find((widget) => {
			if (widget.type === "org.libli.v0.widget.profile") {
				return widget;
			}
		});
		const stylesWidget = widgets.find((widget) => {
			if (widget.type === "org.libli.v0.widget.styles") {
				return widget;
			}
		});
		if (stylesWidget) {
			this.renderWidgetStyles(stylesWidget);
		}

		if (profileWidget) {
			this.profileType = profileWidget.content;
		} else {
			this.profileType = "default";
		}
	}

	/* lifecycle */
	constructor() {
		super();
		this.addEventListener("mxready", this.onMatrixReady.bind(this));
		this.addEventListener("mxeventselect", this.onEventSelect.bind(this));
		this.addEventListener("mxcontext", this.onContext.bind(this));
	}
	connectedCallback() {
		this.initAttributes();
		this.render();
	}
	disconnectedCallback() {
		this.removeEventListener("mxready", this.onMatrixReady);
		this.removeEventListener("mxeventselect", this.onEventSelect);
		this.removeEventListener("mxcontext", this.onContext);
	}
	initAttributes() {
		this.removeAttribute("profile-type");
	}
	render() {
		if (this.profileFull?.startsWith("@")) {
			const $userProfile = this.createUserProfile();
			this.append($userProfile);
		} else {
			const $roomProfile = this.createRoomProfile();
			this.append($roomProfile);
		}
	}
	renderWidgetStyles(libliWidgetStyle) {
		const $styles = this.createWidgetStyles(libliWidgetStyle);
		this.append($styles);
	}
	createRoomProfile() {
		const $roomEl = document.createElement("matrix-room");
		this.origin && $roomEl.setAttribute("origin", this.origin);
		$roomEl.setAttribute("profile-id", this.profileFull);

		/* filter all known "playable" event types */
		$roomEl.setAttribute(
			"filter",
			JSON.stringify({
				types: ["m.room.message", "org.libli.room.message.track"],
				not_types: [
					"m.room.redaction",
					"m.room.member",
					"libli.widget",
					"im.vector.modular.widgets",
					"im.vector.modular.widgets.libli.widget",
					"m.reaction",
					"m.room.history_visibility",
					"m.room.power_levels",
					"m.room.join_rules",
					"m.room.canonical_alias",
					"m.room.guest_access",
					"m.room.create",
					"m.room.name",
					"m.room.topic",
					"m.space.child",
					"m.room.avatar",
				],
			}),
		);
		$roomEl.setAttribute(
			"send-event-types",
			JSON.stringify(["m.room.message", "org.libli.room.message.track"]),
		);
		/* only show space children */
		$roomEl.setAttribute("show-space-children", true);
		$roomEl.setAttribute("show-space-parents", true);
		$roomEl.setAttribute("show-send-event", true);

		/* some config;
		 for examples, @usernames are hidden with CSS on some room types */
		$roomEl.setAttribute("show-room-actions", true);
		$roomEl.setAttribute("show-event-sender", true);

		/* set the event-id, show context or not */
		if (this.context) {
			$roomEl.setAttribute("event-id", this.context);
			$roomEl.setAttribute("show-context", true);
			$roomEl.setAttribute("context-limit", 100);
		} else if (this.eventId) {
			$roomEl.setAttribute("event-id", this.eventId);
		} else {
			$roomEl.setAttribute("show-context", true);
		}

		if (canSanitize) {
			$roomEl.setAttribute("flag-sanitizer", true);
		}

		/* by default, we show info, but hide visually w/ css */
		$roomEl.setAttribute("show-event-info", true);
		$roomEl.setAttribute("show-event-actions", true);

		return $roomEl;
	}
	createUserProfile() {
		const $profileLayout = document.createElement("page-profile-user");
		const $matrixProfile = document.createElement("matrix-profile");
		$matrixProfile.setAttribute("user-id", this.profileFull);

		const $link = document.createElement("a");
		$link.setAttribute("href", `https://matrix.to/#/${this.profileFull}`);
		$link.setAttribute("target", "_blank");
		$link.append($matrixProfile);

		$profileLayout.append($link);
		return $profileLayout;
	}

	createWidgetStyles(widget) {
		const $styles = document.createElement("style");
		$styles.textContent = widget.content;
		return $styles;
	}
}
