import exampleRooms from "../utils/example-rooms.js";
import {
	widgetExists,
	getWidgetHTMLElement,
	LIBLI_WIDGET_SUBTYPE,
	WIDGETS_TYPES_DOM,
	WIDGET_CONTENT_KEY,
	decodeConfig,
	encodeConfig,
	buildConfigUrl,
} from "../services/libli-widgets.js";

export default class PageWidgets extends HTMLElement {
	static get observedAttributes() {
		return ["origin", "hash", "type", "config"];
	}
	get origin() {
		return this.getAttribute("origin");
	}
	get type() {
		// first comes from query param, second router url path
		return this.getAttribute("type") || this.getAttribute("widget-type");
	}
	get config() {
		let decodedConfig;
		try {
			decodedConfig = decodeConfig(this.getAttribute("config"));
		} catch (e) {
			console.info("Could not decode config", e);
		}
		return decodedConfig;
	}

	/* attributes from a matrix client when embeded as widget there (see router )*/
	get roomId() {
		/* matrix_room_id from matrix client as widget, or "libli pinned hash" */
		return (
			this.getAttribute("matrix-room-id") ||
			this.getAttribute("hash") ||
			exampleRooms.widgetPreview
		);
	}

	/* state helpers */
	get widgetAsMockEvent() {
		return {
			content: {
				data: {
					type: `${LIBLI_WIDGET_SUBTYPE}.${this.type}`,
					title: `libli.${this.type}`,
					url: `${this.origin}/widgets?type=${this.type}&config=${encodeConfig(
						this.config,
					)}`,
				},
				type: "m.custom",
				name: "Custom Libli Widget Preview",
			},
			room_id: "abc123",
			event_id: "567def",
			user_id: "@example:sctlib.gitlab.org",
		};
	}

	/* dom helpers */
	get $pageProfile() {
		return this.querySelector("libli-widget-preview page-profile");
	}

	/* all the known widgets */
	get widgets() {
		return Object.keys(WIDGETS_TYPES_DOM);
	}

	/* the root name of the HTML element used in widgets (<libli-widget-profile/>)*/
	widgetRoot = "libli-widget";

	/* events (only for the room preview) */
	onPreviewMatrixReady(event) {
		if (this.$pageProfile) {
			this.$pageProfile.handleMatrixWidgets([this.widgetAsMockEvent]);
		}
	}
	onWidgetSubmit(event) {
		// always from a form submit event
		event.preventDefault();
		this.getWidgetFormContent(event.target);
	}

	getWidgetFormContent(widgetForm) {
		const data = new FormData(widgetForm);
		const widgetType = "test";
		const widgetContent = data.get(WIDGET_CONTENT_KEY);
		if (widgetContent) {
			const url = buildConfigUrl({
				type: this.type,
				origin: this.origin,
				content: widgetContent,
			});
			this.copyToClipboard(url);
		} else {
			this.copyToClipboard("");
		}
	}
	copyToClipboard(text) {
		navigator.clipboard.writeText(text);
	}

	connectedCallback() {
		document.title = "Widgets";
		if (this.config) {
			this.renderTypeConfig();
		} else if (widgetExists(this.type)) {
			/* if a "type of widget" is specified in URL, only render this one widget */
			this.renderEmptyType();
		} else {
			this.renderTypeSelection();
		}
	}
	renderEmptyType(widgetType) {
		const $widget = this.createWidgetType({
			type: this.type,
			origin: this.origin,
		});
		this.replaceChildren($widget);
	}
	renderTypeSelection() {
		const $intro = this.createWidgetsIntro();
		const $menu = this.createWidgetsMenu();
		this.replaceChildren($intro, $menu);
	}
	renderTypeConfig() {
		const $widget = this.createWidgetType({
			config: this.config,
			type: this.type,
			origin: this.origin,
		});
		const $widgetWrapper = this.createWidgetPreviewWrapper(
			`libli.${this.type} (widget config)`,
		);
		$widgetWrapper.append($widget);
		const $widgetPreviewWrapper = this.createWidgetPreviewWrapper("preview");

		if (this.roomId) {
			const $preview = this.createPreview(this.config, this.roomId);
			$widgetPreviewWrapper.append($preview);
		}

		this.replaceChildren($widgetWrapper, $widgetPreviewWrapper);
	}
	createWidgetsIntro() {
		const $intro = document.createElement("header");
		$intro.innerHTML = `
			<details>
				<summary>
					Customize a matrix room with libli widgets
				</summary>
				<p>Each customisation will geneneate a <strong>libli widget URL</strong> to be added as "custom widget" in the room.</p>
				<p><i>Curently, we have to use an other matrix client (ex: element web client), to add a "new custom widget" with the provided "libli widget URL".</i></p>
				<p>Use one widget URL, by libli widget type, by room.</p>
				<p>List of available libli widget types:</p>
			</details>
		`;
		return $intro;
	}
	createWidgetsMenu() {
		const $widgetsMenu = document.createElement("menu");
		const $widgets = this.widgets.map((name) => {
			const $widgetList = document.createElement("li");
			const $widgetLink = document.createElement("a");
			$widgetLink.setAttribute("href", `${this.origin}/widgets/${name}`);
			$widgetLink.textContent = name;
			$widgetList.append($widgetLink);
			return $widgetList;
		});
		$widgetsMenu.append(...$widgets);
		return $widgetsMenu;
	}
	createWidgetType({ type, config, origin }) {
		const widgetHTMLElement = getWidgetHTMLElement(type);
		const $widget = document.createElement(widgetHTMLElement);
		$widget.addEventListener("submit", this.onWidgetSubmit.bind(this));
		origin && $widget.setAttribute("origin", origin);
		/* still an "encoded string" from this app's URL search params */
		config && $widget.setAttribute("content", config.content);
		return $widget;
	}
	createPreview(config, roomId) {
		const $widgetPreview = document.createElement("libli-widget-preview");
		const $widgetContext = document.createElement("page-profile");
		/* on ready we handle the widget for preview
			 (if no roomId, otherwise, the widget is in the room as a libli widget) */
		if (!this.roomId) {
			$widgetContext.addEventListener(
				"mxready",
				this.onPreviewMatrixReady.bind(this),
			);
		}
		$widgetContext.setAttribute("profile-type", config.content);

		const $roomPreview = document.createElement("matrix-room");
		$roomPreview.setAttribute("profile-id", roomId);
		$roomPreview.setAttribute("show-context", true);
		$roomPreview.setAttribute("origin", this.origin);
		$widgetContext.append($roomPreview);
		$widgetPreview.append($widgetContext);

		return $widgetPreview;
	}
	createWidgetPreviewWrapper(summary) {
		const $details = document.createElement("details");
		$details.setAttribute("open", true);
		const $summary = document.createElement("summary");
		$summary.textContent = summary;
		$details.append($summary);
		return $details;
	}
}
