/*
	 a service to work with libli "custom matrix widgets"
 */

import sanitize from "./sanitizer.js";

// URL where libli widget data can be read
const ORIGIN = "https://libli.org";
// matrix widget event types
const MATRIX_CUSTOM_WIDGETS_TYPES = ["m.custom", "customwidget"];
// root of libli widgets (widgetEvent.content.type)
const LIBLI_WIDGET_SUBTYPE = "org.libli.v0.widget";
// widgetEvent.content.content (libli content?)
const WIDGET_CONTENT_KEY = "content";
// mapping of "libli widget types" and their HTML DOM element
const WIDGETS_TYPES_DOM = {
	styles: "libli-widget-styles",
	profile: "libli-widget-profile",
};

/* matrix spec client/widget (?)
	 from a matrix client, as embeded widget (matrix "widget spec" URL)
	 On element client: /addwidget <W_URL?search-parm=$search-param=placeholder>
	 → /addwidget <libli_widget__config_url>&matrix_room_id=$matrix_room_id/;
	 Also ref: https://gitlab.com/sctlib/matrix-widgets
 */
const MATRIX_WIDGET_SEARCH_PARAMS = [
	"matrix_widget_id",
	"matrix_room_id",
	"matrix_user_id",
	"matrix_display_name",
	"matrix_avatar_url",
];

const widgetExists = (name) => {
	return !!WIDGETS_TYPES_DOM[name];
};

const getWidgetHTMLElement = (name) => {
	return WIDGETS_TYPES_DOM[name];
};

const decodeConfig = (config) => {
	let event;
	try {
		event = JSON.parse(atob(config));
	} catch (e) {}
	return event;
};

const encodeConfig = (config = {}) => {
	let event;
	try {
		event = btoa(JSON.stringify(config));
	} catch (e) {}
	return event;
};

const getWidgetsOrigin = (widgetsOrigin) => {
	const widgetsOriginUrl = new URL(widgetsOrigin);
	if (widgetsOriginUrl.protocol === "https") {
		return widgetsOrigin;
	} else {
		return ORIGIN;
		// if "debug" can use http://localhost:4040
		/* return widgetsOrigin; */
	}
};

const filterLibliWidget = ({ content }, platformOrigin) => {
	const widgetsOrigin = getWidgetsOrigin(platformOrigin);
	const { type, data, url } = content;
	if (MATRIX_CUSTOM_WIDGETS_TYPES.includes(type) && data) {
		if (data.url && data.url.startsWith(widgetsOrigin)) {
			return true;
		}
		/* when added with /addwidget in matrix client element */
		if (url && url.startsWith(widgetsOrigin)) {
			return true;
		}
	}
};

const getLiblibWidgetConfig = ({ content }) => {
	const { type, data, url } = content;
	const widgetConfigUrl = new URL(data?.url || url);
	const widgetConfig = decodeConfig(widgetConfigUrl.searchParams.get("config"));
	return widgetConfig;
};

const sanitizeLibliWidget = ({ type, content, timestamp }) => {
	try {
		const saneContent = sanitize(content);
		const saneType = sanitize(type);
		const saneTimestamp = sanitize(timestamp);
		return {
			type: saneType,
			content: saneContent,
			timestamp: saneTimestamp,
		};
	} catch (e) {
		throw e;
	}
};

const getLibliWidgets = (matrixWidgets, widgetsOrigin = ORIGIN) => {
	const libliWidgets = matrixWidgets.filter((widget) => {
		return filterLibliWidget(widget, widgetsOrigin);
	});
	const sanitizedWidgets = libliWidgets.map((widget) => {
		const libliWidgetConfig = getLiblibWidgetConfig(widget);
		return sanitizeLibliWidget(libliWidgetConfig);
	});
	return sanitizedWidgets;
};

const buildConfigUrl = ({ type, content, origin }) => {
	const config = {
		type: `${LIBLI_WIDGET_SUBTYPE}.${type}`,
		content,
		/* not used, why here? */
		timestamp: new Intl.DateTimeFormat("en", {
			dateStyle: "short",
			timeStyle: "long",
		}).format(Date.now()),
	};
	const widgetUrlRoot = `${origin}/widgets`;
	const url = new URL(widgetUrlRoot);
	url.searchParams.set("type", type);
	url.searchParams.set("config", encodeConfig(config));
	const mxWidgetsParams = MATRIX_WIDGET_SEARCH_PARAMS.map((mxWidgetParam) => {
		// values will be set by the matrix client libli is embeded in
		// the 2 `$` are on purpose, as the matrix client will use it as placeholders
		// made as a string to prevent automatic "percent encoding" of search params
		return `${mxWidgetParam}=$${mxWidgetParam}`;
	}).join("&");
	return `${url.href}&${mxWidgetsParams}`;
};

const getClientWidgetParams = (url) => {
	try {
		const matrixWidgetParams = Array.from(new URLSearchParams(url)).reduce(
			(acc, attr) => {
				const [attrName, attrValue] = attr;
				if (MATRIX_WIDGET_SEARCH_PARAMS.includes(attrName)) {
					acc[attrName] = attrValue;
				}
				return acc;
			},
			{},
		);
		return matrixWidgetParams;
	} catch (error) {
		console.log("error");
	}
};

export {
	ORIGIN,
	MATRIX_CUSTOM_WIDGETS_TYPES,
	MATRIX_WIDGET_SEARCH_PARAMS,
	LIBLI_WIDGET_SUBTYPE,
	WIDGETS_TYPES_DOM,
	WIDGET_CONTENT_KEY,
	widgetExists,
	getWidgetHTMLElement,
	decodeConfig,
	encodeConfig,
	filterLibliWidget,
	sanitizeLibliWidget,
	getLibliWidgets,
	buildConfigUrl,
	getClientWidgetParams,
};
