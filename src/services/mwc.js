import mwc from "@sctlib/mwc";
import trackForm from "../templates/matrix-event/org.libli.room.message.track/form.js";
import trackDisplay, {
	onTrackPlay,
} from "../templates/matrix-event/org.libli.room.message.track/display.js";

/* register a custom "track" (embed iframe media) event */
const TRACK_EVENT_TYPE = "org.libli.room.message.track";
const TRACK_EVENT_TYPE_CONFIG = {
	formTemplate: trackForm,
	displayTemplate: trackDisplay,
	/* @TODO:mwc register Display Events? */
	displayEvents: {
		play: onTrackPlay,
	},
};
mwc.eventsManager.registerEventType(TRACK_EVENT_TYPE, TRACK_EVENT_TYPE_CONFIG);

/*
	 @TODO: Implement in mwc
	 local (mock) implementation in libli (to see if it works)
 */
const eventTypesWithDisplayEvents = new Map([
	[TRACK_EVENT_TYPE, TRACK_EVENT_TYPE_CONFIG],
]);

console.info(
	"Registered matrix events (flag.displayEvents — for DOM handlers)",
	mwc.eventsManager.eventTypes,
);

/* EVENT_TYPES(_WITH_DISPLAY_EVENTS) until in mwc */
const EVENT_TYPES = eventTypesWithDisplayEvents || mwc.eventsManager.eventTypes;

const filterWithDisplayEvents = ([eventType, eventConfig]) => {
	return eventConfig?.displayEvents;
};
const displayEventToDomEvent = (displayEvent, eventType) => {
	const [displayEventName, displayEventFn] = displayEvent;
	const displayEventDomEventName = `${eventType}.${displayEventName}`;
	return {
		event_type: eventType,
		display_event_type: displayEventDomEventName,
		name: displayEventName,
		callback: displayEventFn,
	};
};

const _addDisplayEventListener = (
	eventType,
	displayEvents,
	$target,
	domEventConfig,
) => {
	const { stopPropagation, preventDefault } = domEventConfig;
	return Object.entries(displayEvents)
		.map((displayEvent) => {
			return displayEventToDomEvent(displayEvent, eventType);
		})
		.map((displayEvent) => {
			return $target.addEventListener(
				displayEvent.display_event_type,
				(domEvent) => {
					const { detail } = domEvent;
					// prevent default, stop propagation
					if (stopPropagation) {
						domEvent.stopPropagation();
					}
					if (preventDefault) {
						domEvent.preventDefault();
					}
					return displayEvent.callback({
						dom_event: domEvent,
						matrix_event: detail,
						app: $target,
					});
				},
			);
		});
};

/* each matrix event's dom event */
const _removeDisplayEventListener = (eventType, displayEvents, $target) => {
	return Object.entries(displayEvents)
		.map((displayEvent) => {
			return displayEventToDomEvent(displayEvent, eventType);
		})
		.map((displayEvent) => {
			return $target.removeEventListener(
				displayEvent.display_event_type,
				displayEvent.callback,
			);
		});
};

/* (public) each matrix events */
const addDisplayListeners = ($target, domEventConfig = {}) => {
	const customEvents = Array.from(EVENT_TYPES).map((eventType) => {
		const [eventTypeName, eventTypeConfig] = eventType;
		if ($target) {
			_addDisplayEventListener(
				eventTypeName,
				eventTypeConfig.displayEvents,
				$target,
				domEventConfig,
			);
			return eventType;
		}
	});
	console.info(
		"Registered custom matrix DOM events",
		customEvents,
		$target,
		domEventConfig,
	);
	return customEvents;
};

const removeDisplayListeners = ($target) => {
	const customEvents = Array.from(EVENT_TYPES).map((eventType) => {
		const [eventTypeName, eventTypeConfig] = eventType;
		if ($target) {
			_removeDisplayEventListener(
				eventTypeName,
				eventTypeConfig.displayEvents,
				$target,
			);
			return eventType;
		}
	});
	console.info("Un-registered custom matrix DOM events", customEvents, $target);
	return customEvents;
};

/* define components in order */
mwc.defineComponents(mwc.componentDefinitions);

export { mwc as default, addDisplayListeners, removeDisplayListeners };
