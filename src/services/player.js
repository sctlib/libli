const PLAYER_EVENTS = {
	TRACK_ENDED: "eventEnded",
};

const { TRACK_ENDED } = PLAYER_EVENTS;

class LibliPlayerService {
	constructor() {
		this.players = new Map();
	}
	registerEventPlayer(
		eventType,
		{ load, createElement, playEvent, onEventEnded },
	) {
		this.players.set(eventType, {
			load,
			createElement,
			playEvent,
			onEventEnded,
		});
	}
}

/* create a singleton instance of the manager */
const playerService = new LibliPlayerService();

playerService.registerEventPlayer("org.libli.room.message.track", {
	load: async ($libliPlayer, matrixEvent) => {
		if (!window.radio4000Player) {
			try {
				await import("https://cdn.jsdelivr.net/npm/radio4000-player");
			} catch (e) {
				console.error("Error loading radio4000-player from cdn", e);
			}
		}

		/* register custom player events */
		const $r4 = document.createElement("radio4000-player");
		$r4.addEventListener("trackEnded", ({ detail }) => {
			$r4.dispatchEvent(
				new CustomEvent(`${matrixEvent.type}.${TRACK_ENDED}`, {
					bubbles: true,
					detail,
				}),
			);
		});

		/* trick to load a player ready when in the dom */
		$libliPlayer.$layout.append($r4);
		return new Promise((resolve) => {
			$r4.addEventListener("playerReady", resolve);
		});
	},
	createElement: (matrixEvent) => {
		// not-left empty (even if inserted in load)
		// because used to find player by tagName in libli-player
		return document.createElement("radio4000-player");
	},
	playEvent: (matrixEvent, eventPlayer) => {
		if (typeof eventPlayer.getVueInstance === "function") {
			const vueInstance = eventPlayer.getVueInstance();
			const { event_id, content } = matrixEvent;
			const { title, url, description } = content;
			const track = {
				title,
				url,
				body: description,
				id: event_id,
			};
			if (event_id && url) {
				/* vueInstance.pause(); // not working */
				try {
					vueInstance.track = {};
					vueInstance.tracks = {};
					vueInstance.updatePlaylist();
				} catch (e) {}
				vueInstance.updatePlaylist({
					tracks: [track],
				});
				vueInstance.play();
			} else {
				eventPlayer.dispatchEvent(
					new CustomEvent(`${matrixEvent.type}.${TRACK_ENDED}`, {
						bubbles: true,
						detail: track,
					}),
				);
			}
		}
	},
	onEventEnded(playerEvent) {
		/* should return the "matrix event_id" */
		if (playerEvent.length) {
			const endedEvent = playerEvent[0];
			return endedEvent?.track?.id;
		}
	},
});

export { playerService as default, LibliPlayerService, PLAYER_EVENTS };
