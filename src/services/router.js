/*
	 Docs: https://github.com/visionmedia/page.js
	 Note: could maybe be replaced by URLPattern API
 * also see: https://developer.mozilla.org/en-US/docs/Web/API/URLPattern#see_also */

import page from "page";

// https://github.com/visionmedia/page.js/issues/537
page.configure({ window: window });

/* remove the # from room?
	 should actually do the opposite
 */
const getHashPath = ({ hash, pathname }) => {
	let profileId;
	if (hash.charAt(0) === "#") {
		profileId = hash.substring(1);
	} else {
		profileId = hash;
	}
	const hashPath = pathname + profileId;
	return hashPath;
};

const clientAllowsURL = ({
	roomId,
	homeserver,
	homeserverAuthorized,
	hash,
	hashPin,
}) => {
	if (hash && hashPin) {
		if (hash !== roomId) {
			return false;
		}
	}

	/* check for domain */
	const [alias, domain] = roomId.split(":");
	homeserver && homeserverAuthorized.push(homeserver);
	const requestedHomeserverHostname = new URL(`https://${domain}`).hostname;
	if (homeserverAuthorized === "*") {
		return true;
	} else if (homeserverAuthorized.length && homeserverAuthorized[0] === "*") {
		return true;
	}

	const authorized = homeserverAuthorized
		.map((homeserverUrl) => {
			try {
				return new URL(homeserverUrl).hostname;
			} catch (e) {
				console.error("Invalid authorized-domain URL");
			}
		})
		.filter((h) => h);

	if (
		authorized.length &&
		authorized.indexOf(requestedHomeserverHostname) > -1
	) {
		return true;
	} else if (!domain) {
		return true;
	}
};

export { page, getHashPath };
