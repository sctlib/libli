/* polyfill the Sanitizer until it exists */
import DOMPurify from "dompurify"
window.Sanitizer = DOMPurify;

const Sanitizer = window?.Sanitizer;
const sanitize = Sanitizer?.sanitize;
const canSanitize = typeof Sanitizer === "function";

export { sanitize as default, canSanitize, DOMPurify };
