/* the html element used in the markup (to re-use everywhere in custom elements) */
const htmlTagName = "libli-mx-track";

/* our template, to "display" the content of an event of this type */
const template = document.createElement("template");
template.innerHTML = `
	<${htmlTagName}></${htmlTagName}>
`;

const onTrackPlay = (displayDomEvent) => {
	const { dom_event, matrix_event, app } = displayDomEvent;
	console.log("track.display.onTrackPlay", dom_event, matrix_event, app);
	app.playEvent(matrix_event);
};

/* the web components definition, to display a "media track" as we want it */
export class LibliMatrixTrack extends HTMLElement {
	get event() {
		return JSON.parse(this.getAttribute("event"));
	}
	onPlay(event) {
		const playEvent = new CustomEvent(`${this.event.type}.play`, {
			bubbles: true,
			detail: this.event,
		});
		this.dispatchEvent(playEvent);
		// @NOTE: could imagine "registering display actions"
		// so "play" can appear in the dropdown of this event's actions
	}
	connectedCallback() {
		this.render();
	}
	render() {
		this.innerHTML = "";
		const $doms = this.createDoms();
		this.append(...$doms);
	}
	createDoms() {
		const { title, url, body, discogs_url } = this.event?.content || {};
		const $doms = [];

		const $url = document.createElement(`${htmlTagName}-url`);
		const $urlLink = document.createElement("a");
		$urlLink.setAttribute("href", url);
		$urlLink.setAttribute("target", "_blank");
		$urlLink.setAttribute("rel", "noref noopener");
		$urlLink.innerText = title;
		$url.append($urlLink);
		$doms.push($url);

		if (discogs_url) {
			const $discogsUrl = document.createElement(`${htmlTagName}-discogs-url`);
			const $discogsUrlLink = document.createElement("a");
			$discogsUrlLink.setAttribute("href", url);
			$discogsUrlLink.setAttribute("target", "_blank");
			$discogsUrlLink.setAttribute("rel", "noref noopener");
			$discogsUrlLink.innerText = title;
			$discogsUrl.append($discogsUrlLink);
			$doms.push($discogsUrl);
		}

		if (body) {
			const $body = document.createElement(`${htmlTagName}-body`);
			$body.innerText = body;
			$doms.push($body);
		}

		const $actions = document.createElement(`${htmlTagName}-actions`);
		const $buttonPlay = document.createElement("button");
		$buttonPlay.addEventListener("click", this.onPlay.bind(this));
		$buttonPlay.setAttribute("title", "play");
		$buttonPlay.innerText = "►";
		$actions.append($buttonPlay);
		$doms.push($actions);

		return $doms;
	}
}
/* auto define the element, so not part of the main code, but can be
imported from many places */
if (!customElements.get(htmlTagName)) {
	customElements.define(htmlTagName, LibliMatrixTrack);
}

export { template as default, onTrackPlay };
