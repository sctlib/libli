const defineComponents = (definitions = componentDefinitions) => {
	if (typeof window !== "undefined") {
		Object.entries(definitions).forEach(([cTag, cDef]) => {
			if (!customElements.get(cTag)) {
				customElements.define(cTag, cDef);
			}
		});
	}
};

export { defineComponents };
