import { resolve } from "path";
import { defineConfig } from "vite";

export default defineConfig({
	base: "./",
	build: {
		/* minify: true, */
		outDir: "dist-lib",
		lib: {
			entry: resolve("src/index.js"),
			formats: ["es"],
			name: "libli",
			fileName: "index",
		},
	},
});
