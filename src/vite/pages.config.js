// vite.config.js
import { resolve } from "path";
import { defineConfig } from "vite";
import { existsSync } from "fs";

/* treat the path '/example:matrix.org' as SPA (serve the index.html file, let js handle URL route) */
const vitePluginMatrixId = (options) => ({
	name: "vite-plugin-matrix-id",
	configureServer(server) {
		server.middlewares.use((req, res, next) => {
			const isLocalFile = existsSync(resolve(__dirname, req.originalUrl));
			const isIndex = req.originalUrl.startsWith("/index.html");
			const isVite = req.originalUrl.startsWith("/@vite/");
			const isSource = req.originalUrl.startsWith("/src/");
			const isIndexJs = req.originalUrl.startsWith("/index.js");
			const isNodeModules = req.originalUrl.startsWith("/node_modules/");
			const isDotFile = req.originalUrl.startsWith("/.");
			const maybeMatrixId =
				[
					isDotFile,
					isLocalFile,
					isNodeModules,
					isIndex,
					isVite,
					isSource,
					isIndexJs,
				].filter((cond) => cond).length === 0;

			if (maybeMatrixId) {
				console.info(req.originalUrl);
				req.url = "/";
			}
			next();
		});
	},
});

export default defineConfig({
	plugins: [vitePluginMatrixId()],
	/* appType: 'mpa', */
	base: "/",
	publicDir: "assets",
	build: {
		/* minify: true, */
		outDir: "dist-pages",
		lib: {
			// Could also be a dictionary or array of multiple entry points
			entry: resolve("src/index.js"),
			formats: ["es"],
			name: "libli",
			// the proper extensions will be added
			fileName: "index",
		},
		rollupOptions: {
			/* out input file to bundle the js & css */
			input: {
				main: resolve("index.html"),
			},
		},
		cssTarget: ["edge120", "firefox121", "chrome120", "safari17.2", "opera106"],
	},
});
